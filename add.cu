#include <iostream>
#include <stdlib.h>
#include <math.h>

__global__
void add(int n, float *x, float *y) {
	for (int i = 0; i < n; i++)
		y[i] = x[i] + y[i];
}

int main(void) {
	int N = 1<<20;
	
	float *x, *y;
	

	    
	if (cudaSuccess != cudaMallocManaged((void **)&x, N*sizeof(float)))
	    std::cout << "Allocation failed" << std::endl;
	
	if (cudaSuccess != cudaMallocManaged(&y, N*sizeof(float)))
	    std::cout << "Allocation failed" << std::endl;
	
	for (int i = 0; i < N; i++) {
		x[i] = 1.0f;
		y[i] = 2.0f;
	}
	
	
	add<<<1, 1>>>(N, x, y);
	
	cudaDeviceSynchronize();
	
	float error = 0.0f;
	for (int i = 0; i < N; i++)
		error = fmax(error, fabs(y[i]-3.0f));
	std::cout << "Error: " << error << std::endl;
	
	cudaFree(x);
	cudaFree(y);
	return 0;
}
